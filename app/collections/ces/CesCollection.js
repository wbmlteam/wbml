define([
  'underscore',
  'backbone',
  'models/ces/CesModel'
], function(_, Backbone, CesModel) {

  var CesCollection = Backbone.Collection.extend({
    model: CesModel
  });

  return CesCollection;

});
