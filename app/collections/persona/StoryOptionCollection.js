define([
  'underscore',
  'backbone',
  'models/persona/StoryOptionsModel'
], function(_, Backbone, StoryOptionsModel) {

  var StoryOptionCollection = Backbone.Collection.extend({
    model: StoryOptionsModel
  });

  return StoryOptionCollection;

});
