require.config({
  paths: {
    jquery: '../bower_components/jquery/dist/jquery.min',
    underscore: '../bower_components/underscore/underscore',
    backbone: '../bower_components/backbone/backbone',
    queryString: '../bower_components/backbone-query-parameters/backbone.queryparams.min',
    fullPage: '../bower_components/fullpage.js/jquery.fullPage',
    paper: '../bower_components/paper/dist/paper-core',
    templates: '../templates'
  }
});

require([
  // Load our app module and pass it to our definition function
  'app',
], function(App){
  App = Object.assign({
    Views: {},
    Extensions: {},
    Router: null,
  }, App);
  App.initialize();
});
