define([], function() {
  return {
      personal: [
        {
          name: 'Paul',
          role: 'woifjwofjieofjio',
          storypoints: [
            {
              subTitle: "Meet",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
            },
            {
              title: "Possibilty",
              subTitle: "Jane's Growth Signals",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
              sideBar: {
                title: "Pin Point Marketing",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              }
            },
            {
              title: "Grows",
              subTitle: "Jane's Business",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
              sideBar: {
                title: "Pin Point Marketing 2",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              }
            }
          ]
        },
        {
          name: 'Caitlin',
          role: 'woifjwofjieofjio',
          storypoints: [
            {
              subTitle: "Meet",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
            },
            {
              title: "Possibilty",
              subTitle: "Jane's Growth Signals",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
              sideBar: {
                title: "Pin Point Marketing",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              }
            },
            {
              title: "Grows",
              subTitle: "Jane's Business",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
              sideBar: {
                title: "Pin Point Marketing",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              }
            }
          ]
        },
        {
          name: 'Stephen',
          role: 'woifjwofjieofjio',
          storypoints: [
            {
              subTitle: "Meet",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
            },
            {
              title: "Possibilty",
              subTitle: "Jane's Growth Signals",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
              sideBar: {
                title: "Pin Point Marketing",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              }
            },
            {
              title: "Grows",
              subTitle: "Jane's Business",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
              sideBar: {
                title: "Pin Point Marketing",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              }
            }
          ]
        }
      ],

      business: [
        {
          name: 'Jane',
          role: 'wioejfoejwewoije',
          storypoints: [
            {
              subTitle: "Meet",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
            },
            {
              title: "Possibilty",
              subTitle: "Jane's Growth Signals",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
              sideBar: {
                title: "Pin Point Marketing",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              }
            },
            {
              title: "Grows",
              subTitle: "Jane's Business",
              content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              icon: "",
              sideBar: {
                title: "Pin Point Marketing",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque facilisis id turpis at molestie. Etiam ligula erat, vehicula vitae convallis ac, suscipit egestas neque. Cras consequat vitae massa id lacinia. Nunc tincidunt aliquam tortor eu porta. Nullam sollicitudin enim eu ipsum congue, ac dictum lorem dapibus. Quisque ac viverra metus, a sagittis purus. Donec bibendum placerat nisl, non suscipit est maximus a. Donec consectetur at ligula eget maximus.",
              }
            }
          ]
        }
      ]
  };
});
