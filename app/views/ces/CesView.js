define([
  'jquery',
  'underscore',
  'backbone',
  'paper',
  'data/CesData',
  'collections/ces/CesCollection',
  'models/ces/CesModel',
  'text!/app/templates/ces/cesTemplate.html'
], function($, _, Backbone, Paper, CesData, CesCollection, CesModel, CesTemplate){

  var CesView = Backbone.View.extend({
    el: $("#journey"),
    selectedOption: undefined,
    detailsViewId: "#ces-details-section",

    initialize: function(options){
      var self = this;
      this.collection = new CesCollection(CesData.products);
    },

    render: function(options){

      /*if (options.page === true) {
        this.$el.addClass('page');
      }*/

      var compiledTemplate = _.template( CesTemplate ); //prerender template
      this.$el.find("#ces").append(compiledTemplate({}));
      this.$el.find("#ces " + this.detailsViewId).css({
        display: "none"
      });
      this.prepareCanvas();
      return this;
    },

    prepareCanvas: function() {
      var self = this;
      var pointTrasitioning = false;
      var canvas = document.getElementById('ces-canvas');

      Paper.setup(canvas);

      // eight point circle
      var hexagon = new Paper.Path({closed: true});

      hexagon.strokeColor = 'black';

      var points = 50;

      var radius = 200;

      var angle = ((2 * Math.PI) / points);

      for (i = 0; i <= points; i++) {

        hexagon.add(new Paper.Point(
          radius * Math.cos(angle * i),
          radius * Math.sin(angle * i)
        ));
      }

      var arc = new Paper.Path();
      arc.strokeColor = 'red';
      arc.strokeWidth = 10;
      arc.pivot = hexagon.bounds.center;

      var arcPoints = 3;
      var radius2 = 200;
      var angle2 = (Math.PI / points);

      for (i = 0; i <= 20; i++) {
        arc.add(new Paper.Point(
          radius2 * Math.cos(angle2 * i),
          radius2 * Math.sin(angle2 * i)
        ));
      }

      var arcPivotPoint = new Paper.Point(Paper.view.center.x, Paper.view.center.y);
      arc.position.x += Paper.view.center.x;
      arc.position.y += Paper.view.center.y;
      arc.opacity = 0;
      arc.applyMatrix = false;

      hexagon.position.x += Paper.view.center.x;
      hexagon.position.y += Paper.view.center.y;

      // triangle menu

      var hexPoints = hexagon.segments.map(function(segment) {
        return {
          x: segment.point.x,
          y: segment.point.y
        }
      });

      var triangle = new Paper.Path({closed: true});
      triangle.add(new Paper.Point([hexPoints[12].x, hexPoints[12].y]), new Paper.Point([hexPoints[32].x, hexPoints[32].y]), new Paper.Point([hexPoints[46].x, hexPoints[46].y]));
      triangle.strokeColor = "blue";

      var circlePoints = {};
      var optionPoints = {};
      var rectangle = new Paper.Rectangle(new Paper.Point(50, 50), new Paper.Point(150, 100));

      // triangle circle points
      for (i = 0; i <= 2; i++) {
          circlePoints[i] = new Paper.Path.Circle(0, 200, 8);
          circlePoints[i].strokeColor = 'black';
          circlePoints[i].fillColor = 'black';
          circlePoints[i].position.x = triangle.segments[i].point.x;
          circlePoints[i].position.y = triangle.segments[i].point.y;
          circlePoints[i].indexOption = i;


          optionPoints[i] = {};
          optionPoints[i].header = new Paper.PointText({
            point: [circlePoints[i].position.x, circlePoints[i].position.y],
            content: self.collection.models[i].get('name'),
            fillColor: 'black',
            fontFamily: 'Courier New',
            fontWeight: 'bold',
            fontSize: 32
          });

          optionPoints[i].content = new Paper.PointText({
            point: [circlePoints[i].position.x, circlePoints[i].position.y],
            content: self.collection.models[i].get('summary'),
            fillColor: 'black',
            fontFamily: 'Courier New',
            fontWeight: 'bold',
            fontSize: 25,
            justification: 'center',
            opacity: 0
          });


          optionPoints[i].buttonBox = Paper.Path.Rectangle(rectangle);
          optionPoints[i].buttonBox.position.x = circlePoints[i].position.x;
          optionPoints[i].buttonBox.position.y = circlePoints[i].position.y;
          optionPoints[i].buttonBox.fillColor = 'purple';
          optionPoints[i].buttonBox.opacity = 0;

          optionPoints[i].buttonContent = new Paper.PointText({
            point: [optionPoints[i].buttonBox.position.x, optionPoints[i].buttonBox.position.y],
            content: 'click me',
            fillColor: 'black',
            fontFamily: 'Courier New',
            fontWeight: 'bold',
            fontSize: 16,
            justification: 'center',
            opacity: 0
          });


          optionPoints[i].button = new Paper.Group({
            children: [optionPoints[i].buttonBox, optionPoints[i].buttonContent]
          });
      }

      // circle point events
      for (i in circlePoints) {
        (function(index) {
          circlePoints[index].onClick = function(e) {
            if (!pointTrasitioning && self.selectedOption != index) {
              pointTrasitioning = true;
              self.selectedOption = this.indexOption;
              arc.attach('frame', moveArc.bind(this, calculateAnglePosition(circlePoints[self.selectedOption].anglePosition)));
              //optionPoints[index].header.attach('frame', animatefontSize.bind(this, index, 'header'));
              optionPoints[index].content.attach('frame', textOpacity.bind(this, index, 'content'));
              optionPoints[index].buttonBox.attach('frame', textOpacity.bind(this, index, 'buttonBox'));
              optionPoints[index].buttonContent.attach('frame', textOpacity.bind(this, index, 'buttonContent'));
            }
          };
        })(i);
      }

      // button events
      for (i in optionPoints) {
        (function(index) {
          optionPoints[index].button.onClick = function(e) {
            e.preventDefault();
            self.showOptionDetails(index);
          }
        })(i);
      }


      circlePoints[0].anglePosition =  50;
      circlePoints[1].anglePosition = 200;
      circlePoints[2].anglePosition = 300;

      var arcPivotPoint = new Paper.Point(Paper.view.center.x, Paper.view.center.y);
      var arcOffset = 0;
      var arcOpacity = 0;
      var moveArc = function(newAngle, e) {
        if (arc.opacity < 1) {
          arc.opacity = parseInt((arcOpacity += .1).toFixed(1));
        }
        var dir = (newAngle < 0) ? -4 : +4;
        if (Math.abs(arcOffset) <= Math.abs(newAngle)) {
            arcOffset = arcOffset + dir;
            arc.rotate(dir, arcPivotPoint);
        } else {
          arcOffset = 0;
          arc.detach('frame');
        }
      };

      var calculateAnglePosition = function(newAngle) {
        var currentArcAngle = Math.round(arc.matrix.rotation),
            degreeMax = 360,
            angleDiff;

        // find out the direction of rotate
        if (newAngle > currentArcAngle) {
          angleDiff = Math.round(newAngle - currentArcAngle);
          // get the difference
          if (angleDiff > Math.round(degreeMax / 2)) {
            return -(currentArcAngle + (degreeMax - newAngle));
          }
          return angleDiff;
        }
        else {
          angleDiff = Math.round(currentArcAngle - newAngle);
          // get the difference
          if (angleDiff > (degreeMax / 2)) {
            return -(angleDiff + (degreeMax - currentArcAngle));
          }
          return -(angleDiff);
        }
      };

      var contentOpacity = [0, 0, 0];
      var headerOpacity = [0, 0, 0];
      var buttonBoxOpacity = [0, 0, 0];
      var buttonContentOpacity = [0, 0, 0];

      var fontSizeOpts = {
        header: {
          opts: [25, 32],
          current: 0
        }
      };

      var textOpacity = function(index, type) {
        var opacityArr = (type == 'header') ? headerOpacity : contentOpacity;
        if (optionPoints[index][type].opacity < 1) {
          optionPoints[index][type].opacity = parseFloat((opacityArr[index] += .01).toFixed(2));
        } else {
          optionPoints[index][type].detach('frame');
          if (pointTrasitioning) pointTrasitioning = false;
        }

        for (i in optionPoints) {
          (function(idx) {
            if (idx != index) {
              optionPoints[idx][type].opacity = 0;
              opacityArr[idx] = 0;
            }
          })(i);
        }
      }

      /*var animatefontSize = function(index, type) {
        console.log(optionPoints[index][type].fontSize);
        if (optionPoints[index][type].fontSize < fontSizeOpts[type].opts[1]) {
          //console.log((fontSizeOpts[type].current + .1).toFixed(1));
          //optionPoints[index][type].fontSize = parseFloat((fontSizeOpts[type].current + .1).toFixed(1));
        } else {
          optionPoints[index][type].detach('frame');
        }

        for (i in optionPoints) {
          (function(idx) {
            if (idx != index) {
              optionPoints[idx][type].fontSize = fontSizeOpts[type].opts[0];
              fontSizeOpts[type].current = 0;
            }
          })(i);
        }
      }*/

      var target1 = 12;
      var target2 = 32;
      var target3 = 46;

      var pointTransMax1 = 0;
      var pointTransMax2 = 0;
      var pointTransMax3 = 0;

      var reverse1 = false;
      var reverse2 = false;
      var reverse3 = false;

      var steps1 = 200;
      var steps2 = 250;
      var steps3 = 225;

      var dx1 = 0;
      var dy1 = 0;
      var dx2 = 0;
      var dy2 = 0;
      var dx3 = 0;
      var dy3 = 0;

      Paper.view.onFrame = function(e) {



        // check first point 0
        if (Math.round(triangle.segments[0].point.x) == Math.round(hexPoints[target1].x) &&
            Math.round(triangle.segments[0].point.y) == Math.round(hexPoints[target1].y)) {

              if (pointTransMax1 == 3 && !reverse1) {
                reverse1 = true;
              } else if (pointTransMax1 == 0 && reverse1) {
                reverse1 = false;
              }

              if (!reverse1) {
                target1++;
                pointTransMax1++;
              } else {
                target1--;
                pointTransMax1--;
              }

              dx1 = (hexPoints[target1].x - triangle.segments[0].point.x)/steps1;
              dy1 = (hexPoints[target1].y - triangle.segments[0].point.y)/steps1;

        }

        // check first point 1
        if (Math.round(triangle.segments[1].point.x) == Math.round(hexPoints[target2].x) &&
            Math.round(triangle.segments[1].point.y) == Math.round(hexPoints[target2].y)) {

              if (pointTransMax2 == 3 && !reverse2) {
                reverse2 = true;
              } else if (pointTransMax2 == 0 && reverse2) {
                reverse2 = false;
              }

              if (!reverse2) {
                target2++;
                pointTransMax2++;
              } else {
                target2--;
                pointTransMax2--;
              }

              dx2 = (hexPoints[target2].x - triangle.segments[1].point.x)/steps2;
              dy2 = (hexPoints[target2].y - triangle.segments[1].point.y)/steps2;

        }

        // check first point 2
        if (Math.round(triangle.segments[2].point.x) == Math.round(hexPoints[target3].x) &&
            Math.round(triangle.segments[2].point.y) == Math.round(hexPoints[target3].y)) {

              if (pointTransMax3 == 3 && !reverse3) {
                reverse3 = true;
              } else if (pointTransMax3 == 0 && reverse3) {
                reverse3 = false;
              }

              if (!reverse3) {
                target3++;
                pointTransMax3++;
              } else {
                target3--;
                pointTransMax3--;
              }

              dx3 = (hexPoints[target3].x - triangle.segments[2].point.x)/steps3;
              dy3 = (hexPoints[target3].y - triangle.segments[2].point.y)/steps3;

        }

        if (self.selectedOption != 0) {
          triangle.segments[0].point.x += dx1;
          triangle.segments[0].point.y += dy1;
          circlePoints[0].position.x = triangle.segments[0].point.x;
          circlePoints[0].position.y = triangle.segments[0].point.y;
          optionPoints[0].header.position.x = triangle.segments[0].point.x;
          optionPoints[0].header.position.y = triangle.segments[0].point.y + 40;
          optionPoints[0].content.position.x = optionPoints[0].header.position.x;
          optionPoints[0].content.position.y = optionPoints[0].header.bounds.height + optionPoints[0].header.position.y + 20;
          optionPoints[0].buttonBox.position.x = optionPoints[0].header.position.x;
          optionPoints[0].buttonBox.position.y = optionPoints[0].content.bounds.height + optionPoints[0].header.position.y + 80;
          optionPoints[0].buttonContent.position.x = optionPoints[0].buttonBox.position.x;
          optionPoints[0].buttonContent.position.y = optionPoints[0].buttonBox.position.y;
        }

        if (self.selectedOption != 1) {
          triangle.segments[1].point.x += dx2;
          triangle.segments[1].point.y += dy2;
          circlePoints[1].position.x = triangle.segments[1].point.x;
          circlePoints[1].position.y = triangle.segments[1].point.y;
          optionPoints[1].header.position.x = triangle.segments[1].point.x - 180;
          optionPoints[1].header.position.y = triangle.segments[1].point.y - 40;
          optionPoints[1].content.position.x = optionPoints[1].header.position.x;
          optionPoints[1].content.position.y = optionPoints[1].header.position.y + 40;
          optionPoints[1].buttonBox.position.x = optionPoints[1].header.position.x;
          optionPoints[1].buttonBox.position.y = optionPoints[1].content.bounds.height + optionPoints[1].header.position.y + 80;
          optionPoints[1].buttonContent.position.x = optionPoints[1].buttonBox.position.x;
          optionPoints[1].buttonContent.position.y = optionPoints[1].buttonBox.position.y;
        }


        if (self.selectedOption != 2) {
          triangle.segments[2].point.x += dx3;
          triangle.segments[2].point.y += dy3;
          circlePoints[2].position.x = triangle.segments[2].point.x;
          circlePoints[2].position.y = triangle.segments[2].point.y;
          optionPoints[2].header.position.x = triangle.segments[2].point.x + 180;
          optionPoints[2].header.position.y = triangle.segments[2].point.y;
          optionPoints[2].content.position.x = optionPoints[2].header.position.x;
          optionPoints[2].content.position.y = optionPoints[2].header.position.y + 40;
          optionPoints[2].buttonBox.position.x = optionPoints[2].header.position.x;
          optionPoints[2].buttonBox.position.y = optionPoints[2].content.bounds.height + optionPoints[2].header.position.y + 80;
          optionPoints[2].buttonContent.position.x = optionPoints[2].buttonBox.position.x;
          optionPoints[2].buttonContent.position.y = optionPoints[2].buttonBox.position.y;
        }
      }
    },

    showOptionDetails: function(index) {
      var self = this,
          model = this.collection.models[index],
          html =  '<h1>' + model.get('name') + '</h1>'  +
                  '<p>' + model.get('content') + '</p>';

      this
        .$el
        .find("#ces #ces-canvas")
        .animate({'opacity': 0.1}, 100)
        .parent()
        .find(this.detailsViewId + " .ces-details")
        .addClass('animated fadeInUp')
        .html(html)
        .parent()
        .fadeIn(500);
    },

    transitionIn: function(callback) {
      var self = this;

      var transition = function() {
        self.$el.addClass('is-visible');
        self.$el.one('transitionend', function() {
          if (_.isFunction(callback)) {
            callback();
          }
        });
      };

      _.delay(transition, 20);

    },

    transitionOut: function(callback) {
      var self = this;

      self.$el.removeClass('is-visible');
      self.$el.one('transitionend', function() {
        if (_.isFunction(callback)) {
          callback();
        }
      });
    },

    events: {
      'click a#ces-details-close' : 'closeCesDetailsView'
    },

    closeCesDetailsView: function(e) {
      e.preventDefault();
      var self = this;
      this
        .$el
        .find("#ces #ces-canvas")
        .css('opacity', '1')
        .siblings(this.detailsViewId)
        .fadeOut(500)
        .find(".ces-details")
        .html("");
    }
  });

  return CesView;

});
