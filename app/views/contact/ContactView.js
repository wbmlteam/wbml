define([
  'jquery',
  'underscore',
  'backbone',
  'text!/app/templates/contact/contactTemplate.html'
], function($, _, Backbone, ContactTemplate){

  var ContactView = Backbone.View.extend({
    el: $("#journey"),
    initialize: function(options){
      var self = this;
    },

    render: function(options){
      var compiledTemplate = _.template( ContactTemplate ); //prerender template
      this.$el.find("#contact-section").append(compiledTemplate({}));
      return this;
    }
  });

  return ContactView;

});
