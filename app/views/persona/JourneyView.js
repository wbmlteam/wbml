define([
  'jquery',
  'underscore',
  'backbone',
  'fullPage',
  'data/StoryData',
  'models/persona/StoryOptionsModel',
  'views/persona/JourneySidebarView',
  'views/ces/CesView',
  'views/contact/ContactView',
  'text!/app/templates/persona/journeyTemplate.html'
], function($, _, Backbone, fullPage, StoryData, StoryOptionsModel, JourneySidebarView, CesView, ContactView, JourneyTemplate){

  var JourneyView = Backbone.View.extend({
    el: $("#journey"),
    sideBarView: undefined,
    fullPageOptions: {
      scrollingSpeed: 1000,
      fixedElements: '#sidebar',
      normalScrollElements: '#sidebar',
      fadingEffect: true,
      afterRender: function() {
        var $sections = this.find('.section'),
            scrollIndicator = '<a href="#" class="scroll-indicator infinite animated fadeInDown">' +
                                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                              '</a>';
        // add transition to first section
        this.find('.section[data-index=0] .content-wrap').addClass("slide-in");

        // add scroll indicator element to each section
        $sections.each(function(index) {
          if (($sections.length-1) !=  index)
          $(this).append(scrollIndicator);
        });

        // add sidebar to fullpage section
        var $sidebar = $(document).find("#sidebar");
        $sidebar.prependTo("#journey");
        $sidebar
          .hide()
          .find(".content")
          .hide()
          .closest('.content[data-index=0]')
          .show();
      },
      onLeave: function(index, nextIndex, direction) {
        var self = this,
            divNum = (direction == 'up') ? nextIndex-1 : index,
            $sidebar = $("#sidebar");

        // close sidebar if open
        if($sidebar.hasClass("open")) {
          $sidebar.removeClass("open");
        }

        // if first section hide sidebar
        if (divNum == 0 || divNum > 2) {
          $sidebar.hide();
        } else if ($sidebar.css('display') == 'none') {
          $sidebar.show();
        }

        // swap content in sidebar
        $sidebar.find('.content').hide().closest('.content[data-index='+divNum+']').show();

        // remove transition class
        $("#fullpage").find('.section .content-wrap').removeClass("slide-in");
        // add transition class to next section with a delay
        setTimeout(function() {
          $("#fullpage").find('.section[data-index='+divNum+'] .content-wrap').addClass("slide-in");
        }, 200);
      }
    },

    initialize: function(options){
      this.id = options.id;
      this.type = options.type;
      this.model = new StoryOptionsModel(StoryData[this.type][parseInt(this.id)]);
      this.sideBarView = new JourneySidebarView({
        parent: this.$el,
        model: this.model
      });
    },

    render: function(options){
      if (options.page === true) {
        this.$el.addClass('page');
      }

      var data = {data : this.model};
      console.log(data.data.get('storypoints'));
      var compiledTemplate = _.template( JourneyTemplate ); //prerender template
      this.$el.append(compiledTemplate(data));
      this.includeExternalViews();
      this.prepareFullPageMode();
      this.sideBarView.render();
      return this;
    },

    includeExternalViews: function() {
      var cesView = new CesView();
      var contactView = new ContactView();
      cesView.render();
      contactView.render();
    },

    prepareFullPageMode: function() {
      var self = this;
      setTimeout(function() {
        self.$el.find("#fullpage").fullpage(self.fullPageOptions);
      }, 1000);
    },

    transitionIn: function(callback) {
      var self = this;

      var transition = function() {
        self.$el.addClass('is-visible');
        self.$el.one('transitionend', function() {
          if (_.isFunction(callback)) {
            callback();
          }
        });
      };

      _.delay(transition, 20);

    },

    transitionOut: function(callback) {
      var self = this;
      $.fn.fullpage.destroy('all');
      self.$el.removeClass('is-visible');
      self.$el.one('transitionend', function() {
        if (_.isFunction(callback)) {
          callback();
        }
      });
    },

    events: {
      'click a.scroll-indicator': 'scrollDown'
    },

    scrollDown: function(e) {
      e.preventDefault();
      $.fn.fullpage.moveSectionDown();
    }

  });

  return JourneyView;

});
