define([
  'jquery',
  'underscore',
  'backbone',
  'data/StoryData',
  'collections/persona/StoryOptionCollection',
  'models/persona/StoryOptionsModel',
  'text!/app/templates/persona/storyOptionsTemplate.html'
], function($, _, Backbone, StoryData, StoryOptionCollection, StoryOptionsModel, StoryOptionsTemplate){

  var StoryOptionsView = Backbone.View.extend({
    el: $("#story-options"),
    initialize: function(options){
      var self = this;
      this.router = options.router;
      this.type = options.type;
      this.prepareStories(this.type);
    },

    render: function(options){

      if (options.page === true) {
        this.$el.addClass('page');
      }

      var data = {
        _: _,
        data: this.collection.models,
        divNum: Math.floor(12 / this.collection.models.length)
      }
      var compiledTemplate = _.template( StoryOptionsTemplate ); //prerender template
      this.$el.append(compiledTemplate(data));
      return this;
    },

    prepareStories: function(type) {
      this.collection = new StoryOptionCollection(StoryData[type]);
    },

    transitionIn: function(callback) {
      var self = this;

      var transition = function() {
        self.$el.addClass('is-visible');
        self.$el.one('transitionend', function() {
          if (_.isFunction(callback)) {
            callback();
          }
        });
      };

      _.delay(transition, 20);

    },

    transitionOut: function(callback) {
      var self = this;

      self.$el.removeClass('is-visible');
      self.$el.one('transitionend', function() {
        if (_.isFunction(callback)) {
          callback();
        }
      });
    },

    events: {
      'click button.type' : 'goToJourney'
    },

    goToJourney: function(e) {
      e.preventDefault();

      var target = e.currentTarget,
          id = $(e.currentTarget).val();

      this.router.navigate('journey/type/'+this.type+'/id/'+id);
    }

  });

  return StoryOptionsView;

});
