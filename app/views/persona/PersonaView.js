define([
  'jquery',
  'underscore',
  'backbone',
  'models/persona/PersonaOptionsModel',
  'text!/app/templates/persona/personaOptionsTemplate.html'
], function($, _, Backbone, PersonaOptionsModel, PersonaOptionsTemplate){

  var PersonaView = Backbone.View.extend({
    el: $("#persona"),
    initialize: function(options){
      console.log(options);
      this.router = options.router;
      this.model = new PersonaOptionsModel();
    },

    render: function(options){

      if (options.page === true) {
        this.$el.addClass('page');
      }

      var homeTitle = this.model.get('title');
      document.title = homeTitle;
      var homeData = {
        title: homeTitle
      };

      var compiledTemplate = _.template( PersonaOptionsTemplate ); //prerender template
      this.$el.append(compiledTemplate(homeData));
      return this;
    },

    transitionIn: function(callback) {
      var self = this;

      var transition = function() {
        self.$el.addClass('is-visible');
        self.$el.one('transitionend', function() {
          if (_.isFunction(callback)) {
            callback();
          }
        });
      };

      _.delay(transition, 20);

    },

    transitionOut: function(callback) {
      var self = this;

      self.$el.removeClass('is-visible');
      self.$el.one('transitionend', function() {
        if (_.isFunction(callback)) {
          callback();
        }
      });
    },

    events: {
      'click .options a.type': 'displayStoryView'
    },

    displayStoryView: function(e) {
      e.preventDefault();

      var target = e.currentTarget,
          type = $(target).attr('data-type');

      this.router.navigate('story-options/type/'+type);
    }
  });

  return PersonaView;

});
