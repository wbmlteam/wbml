define([
  'jquery',
  'underscore',
  'backbone',
  'text!/app/templates/persona/journeySidebarTemplate.html'
], function($, _, Backbone, JourneySidebarTemplate){

  var JourneyView = Backbone.View.extend({
    el: $("#journey"),

    initialize: function(options){
      this.model = options.model;
    },

    render: function(){
      var data = {data : this.model};
      var compiledTemplate = _.template( JourneySidebarTemplate ); //prerender template
      this.$el.prepend(compiledTemplate(data));
      this.el = this.$el.find('#sidebar');
      return this;
    },

    toggleSidebarContent: function(data) {
      console.log(this.$el);
    },

    events: {
      'click a.toggle-btn' : 'toggleSidebar'
    },

    toggleSidebar: function(e) {
      e.preventDefault();
      this.el.toggleClass('open');

    }

  });

  return JourneyView;

});
