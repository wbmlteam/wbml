define([
  'jquery',
  'underscore',
  'backbone',
  '../navigation/NavigationView',
  'text!/app/templates/home/homeTemplate.html'
], function($, _, Backbone, NavigationView){

  var MainView = Backbone.View.extend({
    el: $("#main-wrapper"),
    router: undefined,
    currentPage: undefined,

    initialize: function(options){
      var self = this;
      self.router = options.router;

      var navigationView = new NavigationView();
      navigationView.render();
    },

    goTo: function(view) {
      var self = this;
      var previous = this.currentPage || undefined;
      var next = view;

      if (previous) {
        previous.transitionOut(function() {
          previous.$el.empty();
          previous.undelegateEvents();
          previous.$el.removeData().unbind();
        });
      }

      next.render({page: true});
      next.transitionIn();
      this.currentPage = next;
    }


  });

  return MainView;

});
