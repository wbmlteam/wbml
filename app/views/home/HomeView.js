define([
  'jquery',
  'underscore',
  'backbone',
  'models/home/HomeModel',
  'text!/app/templates/home/homeTemplate.html'
], function($, _, Backbone,HomeModel,homeTemplate){

  var HomeView = Backbone.View.extend({
    el: $("#home"),

    initialize: function(){
      var self = this;
      self.model = new HomeModel();
    },

    render: function(options){
      if (options.page === true) {
        this.$el.addClass('page');
      }

      var homeTitle = this.model.get('title');
      document.title = homeTitle;
      var homeData = {
        title: homeTitle
      };

      var compiledTemplate = _.template( homeTemplate ); //prerender template
      this.$el.append(compiledTemplate(homeData));
      return this;
    },

    transitionIn: function(callback) {
      var self = this;

      var transition = function() {
        self.$el.addClass('is-visible');
        self.$el.one('transitionend', function() {
          if (_.isFunction(callback)) {
            callback();
          }
        });
      };

      _.delay(transition, 20);

    },

    transitionOut: function(callback) {
      var self = this;

      self.$el.removeClass('is-visible');
      self.$el.one('transitionend', function() {
        if (_.isFunction(callback)) {
          callback();
        }
      });
    }

  });

  return HomeView;

});
