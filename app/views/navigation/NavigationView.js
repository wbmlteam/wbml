define([
  'jquery',
  'underscore',
  'backbone',
  'data/StoryData',
  'text!/app/templates/navigation/navigationTemplate.html',
  'text!/app/templates/navigation/menuTemplate.html'
], function($, _, Backbone, StoryData, NavigationTemplate, MenuTemplate){

  var NavigationView = Backbone.View.extend({
    el: $("#navigation"),
    status: false,

    initialize: function(){
      var self = this;
    },

    render: function(options){

      var navTitle = "FIS";
      var navData = {
        title: navTitle
      };

      var menuData = {
        stories: StoryData
      };

      var compiledTemplate = _.template( NavigationTemplate ); //prerender template
      this.$el.append(compiledTemplate(navData));

      var compiledTemplate2 = _.template( MenuTemplate );
      this.$el.parent().append(compiledTemplate2(menuData));
      return this;
    },

    events: {
      'click button.navbar-toggle': 'toggleMenu'
    },

    toggleMenu: function(e) {
      e.preventDefault();
      var target = e.currentTarget;

      if (!this.status) {
        this.status = true;
        $(target).removeClass("collapsed");
        //show menu
        this.$el.parent().find("#menu").fadeIn(100);
      }

      else {
        $(target).addClass("collapsed");
        this.$el.parent().find("#menu").fadeOut(100);
        this.status = false;

      }
    }

  });

  return NavigationView;

});
