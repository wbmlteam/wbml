// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'queryString',
  'views/main/MainView',
  'views/home/HomeView',
  'views/persona/PersonaView',
  'views/persona/StoryOptionsView',
  'views/persona/JourneyView',
  'views/ces/CesView'
], function($, _, Backbone, QueryString, MainView, HomeView, PersonaView, StoryOptionsView, JourneyView, CesView) {

  var AppRouter = Backbone.Router.extend({
    routes: {
      'home': 'home',
      'persona-options': 'personaOptions',
      'story-options/type/:type': 'storyOptions',
      'journey/type/:type/id/:id': 'showJourney',
      'ces': 'displayCes',

      // Default
      '*actions': 'defaultAction'
    },

    routeParams: {},

    /*
     *Override navigate function
     *@param {String} route The route hash
     *@param {PlainObject} options The Options for navigate functions.
     *              You can send a extra property "params" to pass your parameter as following:
     *              {
     *               params: 'data'
     *              }
     **/
    navigate: function(route, options) {
        var routeOption = {
                trigger: true
            },
            params = (options && options.params) ? options.params : null;
        $.extend(routeOption, options);
        delete routeOption.params;

        //set the params for the route
        this.param(route, params);
        Backbone.Router.prototype.navigate(route, routeOption);
    },

    /*
     *Get or set parameters for a route fragment
     *@param {String} fragment Exact route hash. for example:
     *                   If you have route for 'profile/:id', then to get set param
     *                   you need to send the fragment 'profile/1' or 'profile/2'
     *@param {Any Type} params The parameter you to set for the route
     *@return param value for that parameter.
     **/
    param: function(fragment, params) {
        var matchedRoute;
        _.any(Backbone.history.handlers, function(handler) {
            if (handler.route.test(fragment)) {
                matchedRoute = handler.route;
            }
        });
        if (params !== undefined) {
            this.routeParams[fragment] = params;
        }

        return this.routeParams[fragment];
    }
  });

  var initialize = function(){

    var app_router = new AppRouter,
        mainView = new MainView({ router : this });

    app_router.on({

      'route:home': function () {
        var homeView = new HomeView();
        mainView.goTo(homeView);
      },

      'route:personaOptions': function () {
        var personaView = new PersonaView({
          router: app_router
        });
        mainView.goTo(personaView);
      },

      'route:storyOptions': function (type) {
        var storyOptions = new StoryOptionsView({
          router: app_router,
          type: type
        });
        mainView.goTo(storyOptions);
      },

      'route:showJourney': function (type, id) {
        var journeyView = new JourneyView({
              router: app_router,
              id: id,
              type: type
            });
        mainView.goTo(journeyView);
      },

      'route:defaultAction': function () {
       // We have no matching route, lets display the home page
        var homeView = new HomeView();
        mainView.goTo(homeView);
      },

      'route:displayCes': function () {
        var cesView = new CesView();
        mainView.goTo(cesView);
      }
    });


    Backbone.history.start();
  };
  return {
    initialize: initialize
  };

});
