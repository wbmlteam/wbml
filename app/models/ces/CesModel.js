define([
  'underscore',
  'backbone',
], function(_, Backbone) {

  var CesModel = Backbone.Model.extend({

    defaults : {
      name: 'undefined',
      summary: 'undefined',
      content: 'undefined'
    }

  });

  return CesModel;

});
