define([
  'underscore',
  'backbone',
], function(_, Backbone) {

  var StoryOptionsModel = Backbone.Model.extend({

    defaults : {
      name: 'undefined',
      description: 'undefined'
    }

  });

  return StoryOptionsModel;

});
