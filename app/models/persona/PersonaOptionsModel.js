define([
  'underscore',
  'backbone',
], function(_, Backbone) {

  var PersonaOptionsModel = Backbone.Model.extend({

    defaults : {
        title : 'PERSONA OPTIONS'
    }

  });

  return PersonaOptionsModel;

});
